'use client'
import { useState } from "react"
import Image from "next/image";
import { useSession } from "next-auth/react";
import { usePathname, useRouter } from "next/navigation";

const NFTCard = ({ item, handleEdit, handleDelete }) => {

  const [copied, setCopied] = useState(false);
  const { data: session } = useSession();
  const pathName = usePathname();
  const router = useRouter();

  console.log("Item ", item);

  const handleCopyClick = () => {

  }

  const handleEditLocal = () => {
    // router.push(`/update-prompt?id=${post._id}`)
  }

  return (
    <div
      className="prompt_card">
      <div className="flex justify-between items-start gap-5">
        <div className="flex-1 flex justify-start items-center 
      gap-3 cursor-pointer">
          <Image src={item.image}
            width={40}
            height={40}
            alt="user image"
            className="round-full object-contain"
          />

          <div className="flex flex-col">
            <h3 className="font-satoshi font-semibold text-gray-900">
              {item.name}
            </h3>
          </div>
        </div>

        <div className="copy_btn">
          {item.totalPrice} ETH
        </div>

      </div>

      <p className="my-4 font-satoshi text-sm text-gray-700">
        {item.description}
      </p>
      <p className="font-inter text-sm blue_gradient cursor-pointer">
        {item.price}
      </p>

    </div>

  )
}

export default NFTCard