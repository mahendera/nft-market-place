import { useSession } from 'next-auth/react';
import { useState, useEffect } from 'react';
import NFTCard from './NFTCard';
import { ethers } from "ethers";


const NFTList = ({ marketplace, nft }) => {

    const [loading, setLoading] = useState(true);
    const [nfts, setNfts] = useState([]);


    console.log(marketplace);

    const fetchNfts = async () => {
        let items = [];
        const itemCount = await marketplace.itemCount();
        for (let i = 1; i <= itemCount; i++) {
            const item = await marketplace.items(i);
            if (!item.sold) {

                try {
                    const uri = await nft.tokenURI(item.tokenId);
                    const response = await fetch(uri);
                    const metadata = await response.json();
                    const totalPrice = await marketplace.getTotalPrice(item.itemId);
                    items.push({
                        totalPrice: ethers.utils.formatEther(totalPrice),
                        itemId: item.itemId,
                        name: metadata.name,
                        description: metadata.description,
                        image: metadata.image
                    })
                } catch (err) {
                   console.log(err);
                }
            }
        }
        setNfts(items);
        setLoading(false);
    }


    useEffect(() => {
        fetchNfts();
    }, []);


    return (
        <section className='feed'>
            {loading &&
                <p>Loading NFTS...</p>
            }
            {
                nfts.map(item => <NFTCard item={item} />)
            }
        </section>
    )
}

export default NFTList