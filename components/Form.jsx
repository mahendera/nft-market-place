import Link from 'next/link'
import React from 'react'
import { useState } from 'react';
import { ethers } from "ethers";
import { create as ipfsHttpClient } from 'ipfs-http-client';
const projectId = "2T4rr5VSF96rj18F5p8JCLWisCN";
const projectSecret = "bb34329f2e1b4cbefe69d75e19d6e07d";
const auth =
  "Basic " + Buffer.from(projectId + ":" + projectSecret).toString("base64");


const ipfsClient = ipfsHttpClient({
  host: "ipfs.infura.io",
  port: 5001,
  protocol: "https",
  headers: {
    authorization: auth,
  },
});


const Form = ({ type, nft, marketplace }) => {
  const [submitting, setSubmitting] = useState(false);
  const [nftItem, setNftItem] = useState({
    price: 0,
    name: '',
    description: '',
    image: ''
  });


  const uploadImageToFps = async (event) => {
    event.preventDefault();
    try {
      const file = event.target.files[0];
      console.log("upload file", file);
      if (typeof file !== undefined) {
        const result = await ipfsClient.add(file);
        setNftItem({...nftItem, image:  `https://ipfs.io/ipfs/${result.path}`});
        console.log("nft image ", nftItem);
      }
    } catch (error) {
      console.log("ipfs image upload error", error);
    }
  }

  const createNft = async (e) => {
    e.preventDefault();
    setSubmitting(true);

    try {
      console.log("nft details ", nftItem);
      const result = await ipfsClient.add(JSON.stringify(nftItem))
      console.log("upload json response ", result);
      mintAndMakeNft(result);
    } catch (error) {
      console.log("Error while minting nft", error);
    }

  }

  const mintAndMakeNft = async (result) => {
    let tx = await nft.mint(`https://ipfs.io/ipfs/${result.path}`);
    await tx.wait();
    console.log("Nft minting done")
    const tokenId = await nft.tokenCount();
    console.log("nft toekn id : ", tokenId);
    console.log("marketplace address ", marketplace.address);
    tx = await nft.setApprovalForAll(marketplace.address, true);
    await tx.wait();
    console.log("nft approval done");
    const listingPrice = ethers.utils.parseEther(nftItem.price);
    console.log("listing price and makeitem started", listingPrice);
    tx = await marketplace.makeItem(nft.address, tokenId, listingPrice);
    await tx.wait();
    console.log("minting process completed..")
    setSubmitting(false);
  }




  return (
    <section className="w-full max-w-full flex-start flex-col">
      <h1 className='head_text text-left'>
        <span className='blue_gradient'>{type} NFT</span>
      </h1>
      <p className='desc text-left max-w-md'>
        {type} and share amazing prompts with the
        world, and let your imagination run wild with any AI-powered platform
      </p>
      <form onSubmit={createNft}
        className='mt-10 w-full max-w-2xl flex flex-col gap-7 glassmorphism'>


        <label>
          <span className='font-satoshi font-semibold text-base text-gray-700'>
            NFT Name
            <span className='font-normal'>(#product #webdevelopment #idea)</span>
          </span>
          <input
            value={nftItem.name}
            onChange={(e) => setNftItem({ ...nftItem, name: e.target.value })}

            placeholder="#tag"
            className="form_input"
          />
        </label>

        <label>
          <span className='font-satoshi font-semibold text-base text-gray-700'>
            Description
          </span>
          <textarea
            value={nftItem.description}
            onChange={(e) => setNftItem({ ...nftItem, description: e.target.value })}
            required
            placeholder="Write your prompt here.."
            className="form_textarea"
          />
        </label>

        <label>
          <span className='font-satoshi font-semibold text-base text-gray-700'>
            Price
            <span className='font-normal'>(#product #webdevelopment #idea)</span>
          </span>
          <input
            value={nftItem.price}
            onChange={(e) => setNftItem({ ...nftItem, price: e.target.value })}

            placeholder="#tag"
            className="form_input"
          />
        </label>

        <label>
          <span className='font-satoshi font-semibold text-base text-gray-700'>
            Image
            <span className='font-normal'>(#product #webdevelopment #idea)</span>
          </span>
          <input
            type="file"
            onChange={(e) => uploadImageToFps(e)}
            placeholder="File"
            className="form_input"
          />
        </label>

        <div className='flex-end mx-3 mb-5 gap-4'>
          <Link href="/" className="text-gray-500 text-sm">Cancel</Link>
          <button
            type="submit"
            disabled={submitting}
            className='px-5 py-1.5 text-sm bg-primary-orange rounded-full text-white'>

            {submitting ? `${type}...` : `${type}`}

          </button>
        </div>

      </form>
    </section>
  )
}

export default Form