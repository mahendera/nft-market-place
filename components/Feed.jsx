"use client"

import { useSession } from 'next-auth/react';
import { useState, useEffect } from 'react';
import PromptCard from './NFTCard';

const PromptCardList = ({ data }) => {
  console.log("posts in card list", data);
  return (
    <div className='mt-16 prompt_layout'>
      {
        data.map((post) => {
        return <PromptCard
            key={post._id}
            post={post}
          
          />
        })
      }
    </div>
  );

};


const Feed = () => {

  const { data: session } = useSession();
  const [ searchText, setSearchText ] = useState('');
  const  [allPosts, setAllPosts ] = useState([]);

  console.log("setAllPosts", allPosts)

  const handleSearchChange = (e) => {

  }

  const fetchPosts = async () => {
    const response = await fetch('api/prompt');
    const data = await response.json();
    console.log("posts ", data);
    setAllPosts(data);
  }

  useEffect(() => {
    fetchPosts();
  }, []);


  return (
    <section className='feed'>
      <form className='relative w-full flex-center'>
        <input type="text"
          placeholder='Search for a tag or username'
          value={searchText}
          onChange={handleSearchChange}
          required
          className='search_input peer'
        />
      </form>

      <PromptCardList data={allPosts}/>

      
    </section>
  )
}

export default Feed