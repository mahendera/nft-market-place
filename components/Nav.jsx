"use client"

import Link from "next/link";
import Image from "next/image";
import { useState, useEffect } from "react";
import { signIn, signOut, useSession, getProviders } from "next-auth/react"

const Nav = () => {
    const { data: session } = useSession();
    const [providers, setProviders] = useState(null);
    const [toggleDropDown, setToggleDropDown] = useState(false);
    console.log("session : ", useSession());
    useEffect(() => {
        (async () => {
            const res = await getProviders();
            setProviders(res);
        })();
    }, []);

    return (

        <nav className="flex-between w-full mb-16 pt-3">
            <Link className="flex gap-2 flex-center" href="/">
                <Image src="/assets/images/logo.svg"
                    alt="Promptopia logo"
                    width={30}
                    height={30} />
                <p className="logo_text">Real NFT</p>
            </Link>
            {/* desktop navigation */}
            <div className="sm:flex hidden">
                <div className='flex gap-3 md:gap-5'>
                    <Link href="/create-nft"
                        className="black_btn">
                        Create NFT
                    </Link>
                   
                </div>
                {
                    session?.user ? (
                        <div className='flex gap-3 md:gap-5'>
                            <Link href="/create-prompt"
                                className="black_btn">
                                Create Post
                            </Link>
                            <button type="button" onClick={signOut} className="outline_btn">
                                Sign Out
                            </button>
                            <Link href="/profile">
                                <Image src={session?.user.image}
                                    width={36}
                                    height={36}
                                    className="rounded-full"
                                    alt="Profile" />
                            </Link>
                        </div>
                    ) : (
                        <>
                            {providers && Object.values(providers).map((provider) => (
                                <button
                                    type="button"
                                    key={provider.name}
                                    onClick={() => {
                                        signIn(provider.id);
                                    }}
                                >
                                    Sign In
                                </button>
                            ))}
                        </>
                    )
                }
            </div>

            {/* mobile navigation */}

            <div className="sm:hidden flex relative">
                {session?.user ? (
                    <div className="flex">
                        <Image src="/assets/images/logo.svg"
                            width={36}
                            height={36}
                            className="rounded-full"
                            alt="Profile"
                            onClick={() => setToggleDropDown((prev) => !prev)}
                        />

                        {toggleDropDown && (
                            <div className="dropdown">
                                <Link href="/profile" className="dropdown_link"
                                    onClick={() => setToggleDropDown(false)}>
                                    My Profile
                                </Link>
                                <Link href="/create-prompt" className="dropdown_link"
                                    onClick={() => setToggleDropDown(false)}>
                                    Create Prompt
                                </Link>
                                <button type="button" onClick={() => {
                                    setToggleDropDown(false);
                                    signOut();
                                }}
                                    className="mt-f w-full black_btn">Sign Out</button>
                            </div>
                        )
                        }


                    </div>

                ) : (
                    <>
                        {providers && Object.values(providers).map((provider) => (
                            <button
                                type="button"
                                key={provider.name}
                                onClick={() => signIn(provider.id)}>
                                Sign IN
                            </button>
                        ))}
                    </>
                )
                }

            </div>
        </nav>
    )
}

export default Nav