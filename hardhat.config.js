require("@nomiclabs/hardhat-waffle");

module.exports = {
  solidity: "0.8.4",
  paths: {
    artifacts: "./src/backend/artifacts",
    sources: "./src/backend/contracts",
    cache: "./src/backend/cache",
    tests: "./src/backend/test"
  },

  networks:  {
    infura: {
      url: 'https://sepolia.infura.io/v3/515990693c5448599e3ee427f035c2b7',
      accounts: ['f22c747c3595b4b0f457d48a6e6271c36d54e84e4edfcecae99da3ae1a80d87f']
    }
  }

};