"use client"
import { BlockChainContext } from "./layout";
import NFTList from "@components/NFTList"
import { useContext, useState, useEffect } from 'react';


const Home = () => {

  const {marketplace, nft}  = useContext(BlockChainContext);


  return (
    <section className="w-full flex-center flex-col">
      <h1 className="head_text text-center"> Buy & Sell
        <br className="max-md:hidden" />
        <span className="orange_gradient text-center">NFT</span>
      </h1>
      <p className="desc text-center">Real NFT is a marketplace for indian real estate</p>
      { (marketplace && nft) && <NFTList marketplace={marketplace} nft={nft} />}
    </section>
  )
}

export default Home