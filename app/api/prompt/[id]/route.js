import { connectToDB } from "@utils/database";
import Prompt from "@models/prompt";

export const GET = async (req, { params }) => {
    console.log("Get prompt  ...", params.id);
    try {
        await connectToDB();
        const prompt = await Prompt.findById(params.id).populate('creator')
        console.log("prompt : ", prompt);
        if (!prompt) {
            return new Response("Prompt not found", { status: 404 });
        } else {
            return new Response(JSON.stringify(prompt), { status: 200 });
        }
    } catch (error) { 
        console.log(error);
        return new Response("Failed to fetch prompt", { status: 500 });
    }
}

export const PATCH = async (request, { params }) => {
    const { prompt, tag } = await request.json();
    try{
        await connectToDB();
        const existingPrompt = await Prompt.findById(params.id);
        if(!existingPrompt){
            return new Response("Prompt not foud", {status : 404});
        }else{
            existingPrompt.prompt= prompt;
            existingPrompt.tag=tag;
            await existingPrompt.save()
            return new Response(JSON.stringify(existingPrompt), {status : 200});
        }
    }catch(error){
        console.log("Failed to edit prompt", error);
        return new Response("Failed to update prompt", {status : 500}); 
    }
}

export const DELETE = async (request, { params }) => {
    try{
        await connectToDB();
        await Prompt.findByIdAndRemove(params.id);
        return new Response("Prompt deleted successfully", {status : 200})
    }catch(error){
        console.log("Failed to delete prompt", error);
        return new Response("Failed to delete prompt", {status: 500});
    }
}