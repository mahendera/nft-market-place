'use client'

import React, { useState, useContext } from 'react';
import { useSession } from 'next-auth/react';
import { useRouter } from 'next/navigation';
import Form from '@components/Form';
import { ethers } from 'ethers';
import { BlockChainContext } from '@app/layout';


const CreateNFT = () => {
    const router = useRouter();
    const {marketplace, nft}  = useContext(BlockChainContext);
   

    return (
        <Form
        type="Create"
        nft={nft}
        marketplace={marketplace}
    />
    )
}

export default CreateNFT