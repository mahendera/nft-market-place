'use client'

import React, { useState } from 'react';
import { useSession } from 'next-auth/react';
import { useEffect } from 'react';
import { useRouter, useSearchParams } from 'next/navigation';
import Form from '@components/Form';


const EditPrompt = () => {
    const { data: session } = useSession();
    const router = useRouter();
    const [post, setPost] = useState({
        prompt: '',
        tag: ''
    });
    const [submitting, setSubmitting] = useState(false);
    const searchParams = useSearchParams();
    const promptId = searchParams.get('id');
    console.log("prompt id ", promptId);

    const fetchPrompt = async () => {
        try {
            console.log("Fetching prompt ", promptId);
            const response = await fetch(`/api/prompt/${promptId}`)
            const data = await response.json();
            setPost(data);
        } catch (error) {
            console.log(error);
        }
    }


    const editPrompt = async(e) => {
        e.preventDefault();
        setSubmitting(true);
        try{
            console.log("invoking api to save prompt...");
            const response = await fetch(`/api/prompt/${post._id}`,
            {
                method: 'PATCH',
                body : JSON.stringify({
                    prompt: post.prompt,
                    tag: post.tag,
                    userId : session?.user.id
                }

                )
            })
            if(response.ok){
                router.push("/profile")
            }
        }catch(error){
            console.log(error);
        }
    }

    useEffect(() => {
        console.log("prompt id in useEffect ", promptId);
        if (promptId) {
            console.log("fetch prompt")
            fetchPrompt();
        }
    }, [promptId])

    return (
        <Form
            type="Edit"
            post={post}
            setPost={setPost}
            submitting={submitting}
            handleSubmit={editPrompt}
        />
    )
}



export default EditPrompt