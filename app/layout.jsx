"use client"
import { createContext, useState, useEffect } from 'react';
import Nav from '@components/Nav';
import Provider from '@components/Provider';
import '@styles/global.css';
import { Children } from 'react';
import { ethers } from "ethers";
import MarketplaceAddress from '../contractsData/MarketPlace-address.json';
import Marketplace from '../contractsData/Marketplace.json';
import NFTAddress from '../contractsData/NFT-address.json';
import NFT from '../contractsData/NFT.json';

const BlockChainContext = createContext(null);

const RootLayout = ({ children }) => {



    const [accounts, setAccounts] = useState([]);
    const [bcData, setBcData] = useState({});
    const [nft, setNft] = useState(null);
    const [marketplace, setMarketplace] = useState(null);
    const [loading, setLoading] = useState(true);

    const initializeBlockChain = async () => {
        console.log("initializing block chain");
        const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
        setAccounts(accounts);
        const provider = await new ethers.providers.Web3Provider(window.ethereum);
        const signer = provider.getSigner();
        console.log("signer ", signer);
        loadContracts(signer);
    }

    const loadContracts = async (signer) => {
        const marketplace = new ethers.Contract(MarketplaceAddress.address, Marketplace.abi, signer);
        setMarketplace(marketplace)
        const nft = new ethers.Contract(NFTAddress.address, NFT.abi, signer);
        setNft(nft)
        setBcData({marketplace : marketplace, nft: nft});
        setLoading(false);
    }

    useEffect(() => {
        initializeBlockChain();
    }, []);



    return (
        <html lang='en'>

            <body>
                {loading && <p>Please wait ..</p>}
                {!loading && (
                    <Provider>
                        <BlockChainContext.Provider value={bcData}>
                            <div className="main">
                                <div className="gradient" />
                            </div>
                            <main className='app'>
                                <Nav />
                                {children}
                            </main>
                        </BlockChainContext.Provider>
                    </Provider>
                )
                }
            </body>
        </html>
    )
}

export default RootLayout;
export { BlockChainContext }