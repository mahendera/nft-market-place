import mongoose from 'mongoose';

let isConnected = false;

export const connectToDB= async()=>{
    mongoose.set('strictQuery', true);

    //mahenderakuthota
    //wnyjHuxUIsR3CV8w

    if(isConnected){
        console.log("Mongodb connected");
    }
    
    try{
        console.log(process.env.MONGODB_URL);
        mongoose.connect(process.env.MONGODB_URL,{
            dbName: "share_prompt"
        })
        isConnected=true;
        console.log("Mongodb is connected");
    }catch(error){
        console.log(error)
    }
   
}
