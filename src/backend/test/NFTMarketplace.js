const { expect } = require('chai');

const toWei = (num) => ethers.utils.parseEther(num.toString());
const fromWei = (num) => ethers.utils.formatEther(num);

describe("NFTMarketplace", async () => {
    let deployer, addr1, addr2;
    let nft, marketplace;
    let URI = "Sample URI";

    beforeEach(async () => {
        const NFT = await ethers.getContractFactory("NFT");
        const Marketplace = await ethers.getContractFactory("Marketplace");
        [deployer, addr1, addr2] = await ethers.getSigners();

        // deploy contracts
        nft = await NFT.deploy();
        marketplace = await Marketplace.deploy(3);

    });

    describe("Deployment", () => {

        it("Check deployment of NFT", async () => {
            expect(await nft.name()).to.equal("Real NFT");
            expect(await nft.symbol()).to.equal("REAL");
        });

        it("Check deployment of Marketplace", async () => {
            expect(await marketplace.feePercent()).to.equal(3);
        });
    })

    describe("Minting NFTs", () => {

        it("should track minted nft", async () => {
            await nft.connect(addr1).mint(URI);
            expect(await nft.tokenCount()).to.equal(1);
            expect(await nft.balanceOf(addr1.address)).to.equal(1);
            expect(await nft.tokenURI(1)).to.equal(URI);

            //addr2

            await nft.connect(addr2).mint(URI);
            expect(await nft.tokenCount()).to.equal(2);
            expect(await nft.balanceOf(addr2.address)).to.equal(1);
            expect(await nft.tokenURI(2)).to.equal(URI);
        });

    })


    describe("Making marketplace items", () => {

        beforeEach(async () => {
            await nft.connect(addr1).mint(URI);
            await nft.connect(addr1).setApprovalForAll(marketplace.address, true);
        });

        it("should track newly created item, transfer NFT from seller to marketplace and emit Offered event", async () => {

            expect(await marketplace.connect(addr1).makeItem(nft.address, 1, toWei(1)))
                .to.emit(marketplace, "Offered")
                .withArgs(
                    1,
                    nft.address,
                    1,
                    toWei(1),
                    addr1.address
                )

            expect(await nft.ownerOf(1)).to.equal(marketplace.address)
            expect(await marketplace.itemCount()).to.equal(1);

            const item = await marketplace.items(1);
            expect(item.itemId).to.equal(1);
            expect(item.sold).to.equal(false);
            expect(item.nft).to.equal(nft.address);

        });

        it("should fail if the price is set 0", async () => {
            await expect(
                marketplace.connect(addr1).makeItem(nft.address, 1, 0)
            ).to.be.revertedWith("Price should be greater than 0");
        })

    })

    describe("Purchase marketplace item", () => {
        let price = 2;
        let feePercent=3;
        beforeEach(async () => {
            await nft.connect(addr1).mint(URI);
            await nft.connect(addr1).setApprovalForAll(marketplace.address, true);
            await marketplace.connect(addr1).makeItem(nft.address, 1, toWei(price));
        });

        it("should update item as sold, pay seller, transfer NFT, charge fee  and emit bought event", async () => {
            const sellerInitBal = await addr1.getBalance();
            const feeAccountInitBal = await deployer.getBalance();
            const totalPriceInWei = await marketplace.getTotalPrice(1);

            await expect(
                marketplace.connect(addr2).purchaseItem(1, { value: totalPriceInWei })
            ).to.emit(marketplace, "Bought")
                .withArgs(
                    1,
                    nft.address,
                    1,
                    toWei(price),
                    addr1.address,
                    addr2.address
                 );

        const sellerFinalBal = await addr1.getBalance();
        const feeAccFinalBal = await deployer.getBalance();
        console.log(`seller init bal ${sellerInitBal} and final balance ${sellerFinalBal}`);
        console.log(`market place init bal ${feeAccountInitBal} and final balance ${feeAccFinalBal}`);

        expect(+fromWei(sellerFinalBal)).to.equal(+price+ +fromWei(sellerInitBal));
        const fee = (feePercent/100) * price;
        //expect(fromWei(feeAccFinalBal)).to.equal(+fee+ +fromWei(feeAccountInitBal));

        expect(await nft.ownerOf(1)).to.equal(addr2.address);
        expect( (await marketplace.items(1)).sold).to.equal(true);

    })



})



})